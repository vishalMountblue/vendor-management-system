const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const validationMiddleware = require("../Middleware/validationMiddleware");
const projectSchema = require("../yupValidations/projectValidation");
const project = require("../mongoSchemas/projectsSchema");

router.get("/", async (req, res) => {
  try {
    let response = await mongoose.connection
      .collection("projects")
      .find({ isArchived: { $ne: true } })
      .toArray();
    res.send(response);
  } catch (error) {
    console.log("error---", error);
    res.sendStatus(500);
  }
});

router.post("/", validationMiddleware(projectSchema), async (req, res) => {
  try {
    let response = await project.create(req.body);
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

router.delete("/", async (req, res) => {
  const id = req.query.id;
  try {
    let deleteResponse = await project.findOneAndUpdate(
      { _id: id },
      { $set: { isArchived: true } }
    );
    res.json({ message: "project deleted successfully!" });
  } catch (error) {
    res.send(400);
  }
});

router.put("/", validationMiddleware(projectSchema), async (req, res) => {
  let _id = req.body.id;
  let updatedData = req.body;
  if (!_id) return res.sendStatus(400);

  try {
    let response = await project.findOneAndUpdate({ _id }, updatedData, {
      new: true,
    });
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

module.exports = router;
