const express = require("express");
const router = express.Router();
const resource = require("../mongoSchemas/resourceSchema");
const project = require("../mongoSchemas/projectsSchema");
const projectResourceAllocation = require("../mongoSchemas/projectResourceAllocationSchema");
const projectResourceAllocationSchema = require("../yupValidations/projectResourceAllocation/projectResourceAllocationValidation");
const projectResourceAllocationUpdationSchema = require("../yupValidations/projectResourceAllocation/projectResourceAllocationUpdationValidation");
const validationMiddleware = require("../Middleware/validationMiddleware");
const { ObjectId } = require("mongodb");

router.post(
  "/project/assign",
  validationMiddleware(projectResourceAllocationSchema),
  async (req, res) => {
    let projectId = req.query.project_id;
    let resourceId = new ObjectId(req.body.resource.id);

    if (!projectId && !resourceId)
      res.status(400).json({ message: "Invalid project or resource id" });

    try {
      let resourceResponse = await resource.findOne({ _id: resourceId });
      let projectResponse = await project.findOne({ _id: projectId });

      if (resourceResponse && projectResponse) {
        let projectResourceAllocationObject = {
          resource: {
            id: req.body.resource.id,
            name: req.body.resource.name,
          },
          vendor: {
            id: resourceResponse.vendor_resource_id[0],
            name: resourceResponse.vendor_resource_id[1],
          },
          start_date: req.body.start_date,
          project: {
            id: projectResponse._id,
            name: projectResponse.name,
          },
        };

        let allocationResponse = await projectResourceAllocation.create(
          projectResourceAllocationObject
        );

        res.send(allocationResponse);
      } else {
        res.status(400).json({ message: "Resource not found" });
      }
    } catch (error) {
      res.sendStatus(500);
    }
  }
);

router.get("/project/staff", async (req, res) => {
  let projectId = req.query.project_id;
  if (!projectId)
    return res.status(400).json({ message: "Project id required" });

  try {
    let resourceAllocationResponse = await projectResourceAllocation.find({
      ["project.id"]: projectId,
    });

    if (resourceAllocationResponse.length > 0) {
      let resourceAllocationObject = {
        project: resourceAllocationResponse[0].project,
        resources: [],
      };
      
      resourceAllocationResponse.forEach((resource) => {
        const data = { ...resource._doc };

        delete data.project;

        resourceAllocationObject.resources.push(data);
      });

      res.send(resourceAllocationObject);
    } else {
      res.send(resourceAllocationResponse);
    }
  } catch (error) {
    console.log("error---", error);
    res.sendStatus(500);
  }
});

router.post(
  "/project/unassign",
  validationMiddleware(projectResourceAllocationUpdationSchema),
  async (req, res) => {
    console.log("body", req.body);
    try {
      let unassignAllocationResponse =
        await projectResourceAllocation.findOneAndUpdate(
          {
            _id: req.body.resource_allocation_id,
          },
          { $set: { end_date: req.body.end_date } },
          { new: true }
        );

      res.send(unassignAllocationResponse);
    } catch (error) {
      res.status(400).json({ message: "wrong resource allocation id!" });
    }
  }
);

module.exports = router;
