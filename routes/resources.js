const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const validationMiddleware = require("../Middleware/validationMiddleware");
const resource = require("../mongoSchemas/resourceSchema");
const resourceValidation = require("../yupValidations/resourceValidation");

router.get("/", async (req, res) => {
  try {
    let response = await mongoose.connection
      .collection("resources")
      .find({ isArchived: { $ne: true } })
      .toArray();
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

router.get("/resource", async (req, res) => {
  try {
    let id = req.query.id;
    console.log("id", id);
    let response = await resource.findOne({ _id: id });
    console.log("response---", response);
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

router.post("/", validationMiddleware(resourceValidation), async (req, res) => {
  try {
    let response = await resource.create(req.body);
    console.log("response resource", response);
    res.send(response);
  } catch (error) {
    console.log("error--", error);
    res.status(500).json({ error });
  }
});

router.delete("/", async (req, res) => {
  const id = req.query.id;
  try {
    let deleteResponse = await resource.findOneAndUpdate(
      { _id: id },
      { $set: { isArchived: true } }
    );
    res.json({ message: "resource deleted successfully!" });
  } catch (error) {
    res.send(400);
  }
});

router.put("/", validationMiddleware(resourceValidation), async (req, res) => {
  let _id = req.body.id;
  let updatedData = req.body;
  try {
    let response = await resource.findOneAndUpdate({ _id }, updatedData, {
      new: true,
    });
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

module.exports = router;
