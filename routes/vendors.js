const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const vendor = require("../mongoSchemas/vendorSchema");
const vendorSchema = require("../yupValidations/vendorValidation");
const validationMiddleware = require("../Middleware/validationMiddleware");
const { ObjectId } = require("mongodb");

router.get("/", async (req, res) => {
  try {
    let response = await mongoose.connection
      .collection("vendors")
      .find({ isArchived: { $ne: true } })
      .toArray();
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

router.get("/single-vendor", async (req, res) => {
  try {
    let slug = req.query.slug;
    console.log("slug", slug);
    let response = await mongoose.connection
      .collection("vendors")
      .findOne({ slug });
    console.log("response---", response);
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

router.get("/vendor", async (req, res) => {
  try {
    let id = req.query.id;
    console.log("id", id);
    let response = await vendor.findOne({ _id: id });
    console.log("response---", response);
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

router.post("/", validationMiddleware(vendorSchema), async (req, res) => {
  try {
    let slug = req.body.name.replace(/\s+/g, "-");
    req.body.slug = slug;
    let response = await vendor.create(req.body);
    res.send(response);
  } catch (error) {
    console.log("error---", error);
    res.sendStatus(500);
  }
});

router.delete("/", async (req, res) => {
  const id = req.query.id;
  try {
    let deleteResponse = await vendor.findOneAndUpdate(
      { _id: id },
      { $set: { isArchived: true } }
    );
    res.json({ message: "vendor deleted successfully!" });
  } catch (error) {
    res.send(400);
  }
});

router.put("/", validationMiddleware(vendorSchema), async (req, res) => {
  let _id = req.body.id;
  let slug = req.body.name.replace(/\s+/g, "-");
  req.body.slug = slug;
  let updatedData = req.body;
  try {
    let response = await vendor.findOneAndUpdate({ _id }, updatedData, {
      new: true,
    });
    res.send(response);
  } catch (error) {
    res.sendStatus(500);
  }
});

module.exports = router;
