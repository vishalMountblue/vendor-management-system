const mongoose = require("mongoose");
const resourceSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: true,
  },
  last_name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  phone_number: {
    type: String,
  },
  vendor_resource_id: {
    type: [String],
    required: true,
  },
  isArchived: {
    type: Boolean,
    default: false,
  },
});

const resource = mongoose.model("Resource", resourceSchema);

module.exports = resource;
