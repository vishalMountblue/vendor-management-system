const mongoose = require("mongoose");
const projectResourceAllocationSchema = new mongoose.Schema({
  resource: {
    id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
  },
  vendor: {
    id: {
      type: String,
      required: [true],
    },
    name: {
      type: String,
      required: [true],
    },
  },
  project: {
    id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
  },
  start_date: {
    type: String,
    required: true,
  },
  end_date: {
    type: String,
  },
});

const projectResourceAllocation = mongoose.model("ProjectResourceAllocation",projectResourceAllocationSchema)
module.exports = projectResourceAllocation
