const mongoose = require("mongoose");
const projectSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  short_name: {
    type: String,
    required: true,
  },
  engineering_manager: {
    resource_id: {
      type: String,
      required: true,
    },
    resource: {
      type: String,
      required: true,
    },
  },
  description: {
    type: String,
  },
  dev_url: {
    type: String,
  },
  qa_url: {
    type: String,
  },
  uat_url: {
    type: String,
  },
  live_url: {
    type: String,
  },
  isArchived: {
    type: Boolean,
    default: false,
  },
});

const project = mongoose.model("Project", projectSchema);
module.exports = project;
