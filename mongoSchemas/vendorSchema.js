const mongoose = require("mongoose");
const vendorSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  short_name: {
    type: String,
    required: true,
    unique: true,
  },
  person_of_contact_name: {
    type: String,
  },
  person_of_contact_email: {
    type: String,
  },
  person_of_contact_mobile: {
    type: String,
  },
  slug: {
    type: String,
  },
  isArchived: {
    type: Boolean,
    default: false,
  }
});

const vendor = mongoose.model("Vendor", vendorSchema);

module.exports = vendor;
