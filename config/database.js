require("dotenv").config();
const mongoose = require("mongoose");

const connectMongooseDatabase = async () => {
  try {
    let connection = await mongoose.connect(process.env.url);
    console.log("connection--", connection.connection.host);
  } catch (error) {
    console.log("mongoose error", error);
  }
};

module.exports = connectMongooseDatabase;
