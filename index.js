require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const mongoose = require("mongoose");
const vendorsRoute = require("./routes/vendors");
const resourceRoute = require("./routes/resources");
const projectsRoute = require("./routes/projects");
const resourceAllocationRoute = require("./routes/resourceAllocation")
const connectMongooseDatabase = require("./config/database");
const cors = require("cors");

app.use(bodyParser.json());
app.use(
  cors({
    origin: "http://localhost:5173",
  })
);

connectMongooseDatabase();

app.use(bodyParser.json());
app.use("/vendors", vendorsRoute);
app.use("/resources", resourceRoute);
app.use("/projects", projectsRoute);
app.use("/resourceAllocation",resourceAllocationRoute)

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server started at port ${PORT}`);
});

module.exports = mongoose.connection;
