const yup = require("yup");

const vendorSchema = yup.object({
  name: yup.string().strict().required(),
  short_name: yup.string().strict().required(),
  person_of_contact_name: yup.string().strict(),
  person_of_contact_email: yup.string().strict().email(),
  person_of_contact_mobile: yup
    .string()
    .length(10,"Please provide valid phone number")
    .required(),
});

module.exports = vendorSchema