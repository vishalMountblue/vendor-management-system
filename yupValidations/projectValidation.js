const yup = require("yup");

const projectSchema = yup.object({
  name: yup.string().strict().required(),
  short_name: yup.string().strict().required(),
  engineering_manager: yup.object().shape({
    resource_id: yup.string().strict().required(),
    resource: yup.string("Resource selection required!").strict().required(),
  }),
  description: yup.string().strict(),
  dev_url: yup.string().strict(),
  qa_url: yup.string().strict(),
  uat_url: yup.string().strict(),
  live_url: yup.string().strict(),
});

module.exports = projectSchema;
