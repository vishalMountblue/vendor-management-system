const yup = require("yup");

const resourceSchema = yup.object({
  first_name: yup.string().strict().required(),
  last_name: yup.string().strict().required(),
  email: yup.string().email(),
  vendor_resource_id: yup.array().of(yup.string()).length(2,"Please Select a resource.").required(),
  phone_number: yup
    .string()
    .length(10, "Please provide valid phone number")
});

module.exports = resourceSchema;
