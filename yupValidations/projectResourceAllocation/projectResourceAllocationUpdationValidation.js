const yup = require("yup");

const projectResourceAllocationUpdationSchema = yup.object({
  resource_allocation_id: yup.string().strict().required(),
  end_date: yup.string("End date is required").strict().required(),
});

module.exports = projectResourceAllocationUpdationSchema;