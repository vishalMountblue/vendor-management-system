const yup = require("yup");

const projectResourceAllocationSchema = yup.object({
  resource: yup.object().shape({
    id: yup.string().strict().required(),
    name: yup.string("Resource required!").strict().required(),
  }),
  start_date: yup.string("Date selection required").strict().required(),
});

module.exports = projectResourceAllocationSchema;
